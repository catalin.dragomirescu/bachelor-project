const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')

var corsOptions = {
  origin: 'http://localhost:4200',
  optionsSuccessStatus: 200
}

const app = express();

app.use(bodyParser.json());
app.use(cors(corsOptions));

const authRoutes = require('./routes/auth');
const spotifyRoutes = require('./routes/spotify-api');

app.use(authRoutes);
app.use(spotifyRoutes);

app.listen(8000, () => {
  console.log('Server started at http://localhost:8000/');
});
