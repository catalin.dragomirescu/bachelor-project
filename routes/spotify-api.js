const express = require('express');
const spotifyApiRouter = express();

const request = require('request'); // "Request" library
const querystring = require('querystring');

const predictInterval = require('../services/interval-prediction');

spotifyApiRouter.route('/api/user-details').get((req, res) => {
  const options = {
    url: 'https://api.spotify.com/v1/me',
    headers: { 'Authorization': 'Bearer ' + req.headers.authorization },
    json: true
  };
  
  // use the access token to access the Spotify Web API
  request.get(options, function(error, response, body) {
    if (body.error && body.error.status !== 200) {
      res.status(body.error.status);
      res.send(body);
    } else {
      res.send(body);
    }
  });
});

spotifyApiRouter.route('/api/user-top-artists').get((req, res) => {
  const topArtistsUrl = 'https://api.spotify.com/v1/me/top/artists?';
  const optionsShortTerm = {
    url: topArtistsUrl + querystring.stringify({
      limit: 50,
      time_range: 'short_term'
    }),
    headers: { 'Authorization': 'Bearer ' + req.headers.authorization },
    json: true
  };

  const optionsMediumTerm = {
    url: topArtistsUrl + querystring.stringify({
      limit: 50,
      time_range: 'medium_term'
    }),
    headers: { 'Authorization': 'Bearer ' + req.headers.authorization },
    json: true
  };

  const optionsLongTerm = {
    url: topArtistsUrl + querystring.stringify({
      limit: 50,
      time_range: 'long_term'
    }),
    headers: { 'Authorization': 'Bearer ' + req.headers.authorization },
    json: true
  };

  let topArtists = {
    shortTerm: [],
    mediumTerm: [],
    longTerm: []
  }

  request.get(optionsShortTerm, function(error, response, body) {
    if (body.error && body.error.status !== 200) {  
      res.status(body.error.status);
      res.send(body);
    } else {
      topArtists.shortTerm = body.items;

      request.get(optionsMediumTerm, function(error, response, body) {
        if (body.error && body.error.status !== 200) {  
          res.status(body.error.status);
          res.send(body);
        } else {
          topArtists.mediumTerm = body.items;

          request.get(optionsLongTerm, function(error, response, body) {
            if (body.error && body.error.status !== 200) {  
              res.status(body.error.status);
              res.send(body);
            } else {
              topArtists.longTerm = body.items;
              let favoriteGenres = findFavoriteGenres(topArtists.shortTerm);

              res.send({
                artists: topArtists,
                genres: favoriteGenres
              });
            }
          })
        }
      })
    }
  });
});

spotifyApiRouter.route('/api/user-top-tracks').get((req, res) => {
  const topTracksUrl = 'https://api.spotify.com/v1/me/top/tracks?';

  const optionsShortTerm = {
    url: topTracksUrl + querystring.stringify({
      limit: 50,
      time_range: 'short_term'
    }),
    headers: { 'Authorization': 'Bearer ' + req.headers.authorization },
    json: true
  };

  const optionsMediumTerm = {
    url: topTracksUrl + querystring.stringify({
      limit: 50,
      time_range: 'medium_term'
    }),
    headers: { 'Authorization': 'Bearer ' + req.headers.authorization },
    json: true
  };

  const optionsLongTerm = {
    url: topTracksUrl + querystring.stringify({
      limit: 50,
      time_range: 'long_term'
    }),
    headers: { 'Authorization': 'Bearer ' + req.headers.authorization },
    json: true
  };

  let topTracks = {
    shortTerm: [],
    mediumTerm: [],
    longTerm: []
  }
  
  request.get(optionsShortTerm, function(error, response, body) {
    if (body.error && body.error.status !== 200) {  
      res.status(body.error.status);
      res.send(body);
    } else {
      topTracks.shortTerm = body.items;

      request.get(optionsMediumTerm, function(error, response, body) {
        if (body.error && body.error.status !== 200) {  
          res.status(body.error.status);
          res.send(body);
        } else {
          topTracks.mediumTerm = body.items;

          request.get(optionsLongTerm, function(error, response, body) {
            if (body.error && body.error.status !== 200) {  
              res.status(body.error.status);
              res.send(body);
            } else {
              topTracks.longTerm = body.items;

              res.send(topTracks);
            }
          })
        }
      })
    }
  });
});

spotifyApiRouter.route('/api/manual-filters-recommendations').post((req, res) => {
  const recomandationsUrl = 'https://api.spotify.com/v1/recommendations?';

  const filters = req.body.filters;

  const params = {
    limit: 30
  };

  if (req.body.seedArtists !== 'none') {
    params.seed_artists = req.body.seedArtists;
  }

  if (req.body.seedGenre !== 'none') {
    params.seed_genres = req.body.seedGenre;
  }

  if (filters.acousticness !== -1) {
    params.min_acousticness = Math.max(0, filters.acousticness - 0.15);
    params.max_acousticness = Math.min(1, filters.acousticness + 0.15);
  }

  if (filters.danceability !== -1) {
    params.min_danceability = Math.max(0, filters.danceability - 0.15);
    params.max_danceability = Math.min(1, filters.danceability + 0.15);
  }

  if (filters.energy !== -1) {  
    params.min_energy = Math.max(0, filters.energy - 0.15);
    params.max_energy = Math.min(1, filters.energy + 0.15);
  }

  if (filters.happiness !== -1) {
    params.min_valence = Math.max(0, filters.happiness - 0.15);
    params.max_valence = Math.min(1, filters.happiness + 0.15);
  }

  if (filters.popularity !== -1) {
    params.min_popularity = filters.popularity;
  }

  const options = {
    url: recomandationsUrl + querystring.stringify(params),
    headers: { 'Authorization': 'Bearer ' + req.headers.authorization },
    json: true
  };
  
  request.get(options, function(error, response, body) {
    res.send(body);
  });
});

spotifyApiRouter.route('/api/tracks-based-recommendations').post((req, res) => {
  const audioFeaturesUri = 'https://api.spotify.com/v1/audio-features/?';
  const recomandationsUrl = 'https://api.spotify.com/v1/recommendations?';

  const trackIdList = req.body.trackIdList;
  const seedArtists = req.body.seedArtists;

  const options = {
    url: audioFeaturesUri + querystring.stringify({
      ids: trackIdList
    }),
    headers: { 'Authorization': 'Bearer ' + req.headers.authorization },
    json: true
  };
  
  // use the access token to access the Spotify Web API
  request.get(options, function(error, response, body) {
    let intervalPredictions = predictInterval(body.audio_features);
    console.log(intervalPredictions);

    const recOptions = {
      url: recomandationsUrl + querystring.stringify({
        limit: 30,
        seed_artists: seedArtists,

        min_acousticness: intervalPredictions.acousticness[0],
        max_acousticness: intervalPredictions.acousticness[1],

        min_danceability: intervalPredictions.danceability[0],
        max_danceability: intervalPredictions.danceability[1],

        min_energy: intervalPredictions.energy[0],
        max_energy: intervalPredictions.energy[1],

        min_valence: intervalPredictions.valence[0],
        max_valence: intervalPredictions.valence[1]
      }),
      headers: { 'Authorization': 'Bearer ' + req.headers.authorization },
      json: true
    };

    request.get(recOptions, function(error, response, body) {
      res.send(body);
    });
  });
});

spotifyApiRouter.route('/api/create-playlist').get((req, res) => {
  const userId = req.query.user_id;
  const playlistName = req.query.playlist_name;
  
  const options = {
    url: 'https://api.spotify.com/v1/users/' + userId + '/playlists',
    body: JSON.stringify({
      'name': playlistName
    }),
    headers: { 'Authorization': 'Bearer ' + req.headers.authorization },
    dataType: 'json'
  };
  
  // use the access token to access the Spotify Web API
  request.post(options, function(error, response, body) {
    if (body.error && body.error.status !== 200) {
      res.status(body.error.status);
      res.send(body);
    } else {
      res.send(body);
    }
  });
});

spotifyApiRouter.route('/api/add-tracks-to-playlist').post((req, res) => {

  const options = {
    url: 'https://api.spotify.com/v1/playlists/' + req.body.playlistId + '/tracks',
    body: JSON.stringify({
      'uris': req.body.tracksUris
    }),
    headers: { 'Authorization': 'Bearer ' + req.headers.authorization },
    dataType: 'json'
  };
  
  // use the access token to access the Spotify Web API
  request.post(options, function(error, response, body) {
    if (body.error && body.error.status !== 200) {
      res.status(body.error.status);
      res.send(body);
    } else {
      res.send(body);
    }
  });
});

spotifyApiRouter.route('/api/search').get((req, res) => {

  const options = {
    url: 'https://api.spotify.com/v1/search?' + querystring.stringify({
      q: req.query.search_query,
      type: 'artist',
      limit: 15
    }),
    headers: { 'Authorization': 'Bearer ' + req.headers.authorization },
    dataType: 'json'
  };
  
  request.get(options, function(error, response, body) {
    if (body.error && body.error.status !== 200) {   
      res.status(body.error.status);
      res.send(body);
    } else {
      res.send(body);
    }
  });
});

spotifyApiRouter.route('/api/get-genres').get((req, res) => {
  const options = {
    url: 'https://api.spotify.com/v1/recommendations/available-genre-seeds',
    headers: { 'Authorization': 'Bearer ' + req.headers.authorization },
    dataType: 'json'
  };
  
  request.get(options, function(error, response, body) {
    if (body.error && body.error.status !== 200) {   
      res.status(body.error.status);
      res.send(body);
    } else {
      res.send(body);
    }
  });
});

let findFavoriteGenres = function(topArtists) {
  let genresArrays = topArtists.map(artist => artist.genres);
  let allArtistsGenres = [];
  let uniqueArtistsGenres = [];

  genresArrays.forEach( genres => {
    allArtistsGenres.push(...genres);
  });

  uniqueArtistsGenres = allArtistsGenres.filter((element, index, array) => array.indexOf(element) === index);
  uniqueArtistsGenres = uniqueArtistsGenres.map(currentGenre => {
    let counter = allArtistsGenres.filter(genre => genre === currentGenre).length;
    return {
      genre: currentGenre,
      counter: counter
    };
  });

  uniqueArtistsGenres = uniqueArtistsGenres.sort(function (a, b) {
    return b.counter - a.counter;
  });

  return uniqueArtistsGenres.slice(0,10);
}

module.exports = spotifyApiRouter;
