let predictInterval = function(audioFeatures) {

  let intervalPrediction = {
    danceability:     calcFeatureInterval(mapToValueList(audioFeatures, 'danceability')),
    energy:           calcFeatureInterval(mapToValueList(audioFeatures, 'energy')),
    acousticness:     calcFeatureInterval(mapToValueList(audioFeatures, 'acousticness')),
    valence:          calcFeatureInterval(mapToValueList(audioFeatures, 'valence'))
  }

  return intervalPrediction;
}

let calcFeatureInterval = function(featureValues) {
  let interval;

  let intervalFrequency = [
    0, // 0.0-0.1
    0, // 0.1-0.2
    0, // 0.2-0.3
    0, // 0.3-0.4
    0, // 0.4-0.5
    0, // 0.5-0.6
    0, // 0.6-0.7
    0, // 0.7-0.8
    0, // 0.8-0.9
    0  // 0.9-1.0
  ];

  featureValues.forEach(value => {
    if (value >= 0 && value <= 0.1) {
      intervalFrequency[0]++;
    } else if (value > 0.1 && value <= 0.2) {
      intervalFrequency[1]++;
    } else if (value > 0.2 && value <= 0.3) {
      intervalFrequency[2]++;
    } else if (value > 0.3 && value <= 0.4) {
      intervalFrequency[3]++;
    } else if (value > 0.4 && value <= 0.5) {
      intervalFrequency[4]++;
    } else if (value > 0.5 && value <= 0.6) {
      intervalFrequency[5]++;
    } else if (value > 0.6 && value <= 0.7) {
      intervalFrequency[6]++;
    } else if (value > 0.7 && value <= 0.8) {
      intervalFrequency[7]++;
    } else if (value > 0.8 && value <= 0.9) {
      intervalFrequency[8]++;
    } else if (value > 0.9 && value <= 1) {
      intervalFrequency[9]++;
    }
  });

  let maxInterval = 0;
  let maxIntervalIndex;

  intervalFrequency.forEach((value, index) => {
    if (value > maxInterval) {
      maxInterval = value;
      maxIntervalIndex = index;
    }
  });

  if (maxInterval >= 30) {
    // majority of songs are on this interval
    interval = [maxIntervalIndex / 10 - 0.05, (maxIntervalIndex / 10) + 0.05];
  } else if (maxInterval >= 20) {
    interval = [(maxIntervalIndex / 10) - 0.1, (maxIntervalIndex / 10) + 0.1];
  } else if (maxInterval >= 10) {
    interval = [(maxIntervalIndex / 10) - 0.15, (maxIntervalIndex / 10) + 0.15];
  } else {
    interval = [(maxIntervalIndex / 10) - 0.20, (maxIntervalIndex / 10) + 0.20];
  }

  interval[0] = Math.round(interval[0] * 100) / 100;
  interval[1] = Math.round(interval[1] * 100) / 100;

  interval[0] = Math.max(0, interval[0]);
  interval[1] = Math.min(interval[1], 1);

  return interval;
}

let mapToValueList = function(list, feature) {
  return list.map(item => item[feature]);
}

module.exports = predictInterval;
